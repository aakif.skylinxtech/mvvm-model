//
//  SecondViewController.swift
//  MVVM Model
//
//  Created by macbook on 19/08/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func getUsersBtn(_ sender: Any)
    {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
}
