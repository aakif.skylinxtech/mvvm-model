//
//  ViewController.swift
//  MVVM Model
//
//  Created by macbook on 18/08/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit
import Alamofire

class SecondViewController: UIViewController
{
    @IBOutlet weak var userTV: UITableView!
    
    let viewModel = UserViewModel()
    var userData: [UserData] = []
    var page: Int = 1
    var check = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        getUsers()
    }
    
    func getUsers()
    {
        self.showActivityIndicator()
        viewModel.getUsers(page: self.page) { (status, error) in
            self.hideActivityIndicator()
            
            if (status)
            {
                if (self.viewModel.users?.count == 0)
                {
                    self.check = false
                }
                else
                {
                    self.userData.append(contentsOf: self.viewModel.users!)
                }
                self.userTV.reloadData()
            }
            else
            {
                print(error!)
            }
        }
    }
    
    var activity: FTLinearActivityIndicator {
        if self.view.viewWithTag(99999) is FTLinearActivityIndicator{
            return self.view.viewWithTag(99999) as! FTLinearActivityIndicator
        }else {
            let activity = FTLinearActivityIndicator.init()
            activity.tag = 99999
            return activity
        }
    }
    
    func showActivityIndicator(){
        let ac = self.activity
        DispatchQueue.main.async {
            ac.frame  =  CGRect(x: 0, y:  0, width: 80, height: 10)
            ac.tintColor = UIColor.darkGray
            self.view.addSubview(ac)
            ac.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.height / 2)
            ac.startAnimating()
        }
        
    }
    func hideActivityIndicator(){
        DispatchQueue.main.async {
            let ac = self.activity
            ac.stopAnimating()
            ac.removeFromSuperview()
            self.view.isUserInteractionEnabled = true
        }
    }
    
}

extension SecondViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        self.userData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTVCell") as! UserTVCell
        
        let name = (self.userData[indexPath.row].first_name ?? "") + " " + (self.userData[indexPath.row].last_name ?? "")
        
        let url: URL? = URL(string: self.userData[indexPath.row].avatar ?? "")
        cell.profileImage.downloaded(from: url!)
        cell.name.text = name
        cell.email.text = self.userData[indexPath.row].email ?? ""
        
        if ((indexPath.row == self.userData.count - 1) && check)
        {
            self.page += 1
            getUsers()
        }
        
        return cell
    }
    
    
}

extension UIImageView
{
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
