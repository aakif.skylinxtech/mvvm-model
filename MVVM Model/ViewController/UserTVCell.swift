//
//  UserTVCell.swift
//  MVVM Model
//
//  Created by macbook on 19/08/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import UIKit

class UserTVCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
