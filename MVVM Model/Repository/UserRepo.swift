//
//  UserRepo.swift
//  MVVM Model
//
//  Created by macbook on 18/08/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
import Alamofire

class UserRepo
{
    func getUsers(page: Int, completion: @escaping(UserModel?, String?) -> Void)
    {
        let path = "https://reqres.in/api/users?"
        
        let params = [
            "page": page
            ] as [String : Any]
        
        let request = AF.request(path, method: .get, parameters: params, encoding: URLEncoding.default, headers: APIManager.headers(), interceptor: nil)
        
        request.responseDecodable(of: UserModel?.self) {(resposnse) in
            
            if (resposnse.error == nil)
            {
                let user = resposnse.value
                completion(user!, nil)
            }
            else
            {
                completion(nil, "Network Error!")
            }
        }
    }
}
