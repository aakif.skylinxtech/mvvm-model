

import Foundation
struct UserModel : Codable {
	let page : Int?
	let per_page : Int?
	let total : Int?
	let total_pages : Int?
	let data : [UserData]?
	let ad : UserAd?

	enum CodingKeys: String, CodingKey {

		case page = "page"
		case per_page = "per_page"
		case total = "total"
		case total_pages = "total_pages"
		case data = "data"
		case ad = "ad"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		page = try values.decodeIfPresent(Int.self, forKey: .page)
		per_page = try values.decodeIfPresent(Int.self, forKey: .per_page)
		total = try values.decodeIfPresent(Int.self, forKey: .total)
		total_pages = try values.decodeIfPresent(Int.self, forKey: .total_pages)
		data = try values.decodeIfPresent([UserData].self, forKey: .data)
		ad = try values.decodeIfPresent(UserAd.self, forKey: .ad)
	}

}
