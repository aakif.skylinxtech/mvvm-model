

import Foundation
struct UserAd : Codable {
	let company : String?
	let url : String?
	let text : String?

	enum CodingKeys: String, CodingKey {

		case company = "company"
		case url = "url"
		case text = "text"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		company = try values.decodeIfPresent(String.self, forKey: .company)
		url = try values.decodeIfPresent(String.self, forKey: .url)
		text = try values.decodeIfPresent(String.self, forKey: .text)
	}

}
