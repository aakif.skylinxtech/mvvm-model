//
//  BaseServiceApi.swift
//  Customer App
//
//  Created by Shahbaz Khan on 2/24/20.
//  Copyright © 2020 Shahbaz Khan. All rights reserved.
//

import UIKit

class BaseServiceApi
{
    private let devURL = ""
    private let liveURL = "https://eclinic.skylinxtech.com/api/v1/"
    
    var baseUrl: String! {
        switch NetworkConfiguration.enviroment() {
        case .Live:
            return  liveURL
        case .Testing:
            return  devURL
        case .Stagging:
            return  devURL
        }
        
    }
}

