//
//  UserViewModel.swift
//  MVVM Model
//
//  Created by macbook on 19/08/2020.
//  Copyright © 2020 macbook. All rights reserved.
//

import Foundation
class UserViewModel: NSObject
{
    let repo = UserRepo()
    var users: [UserData]?
    
    func getUsers(page: Int, completion: @escaping(Bool, String?) -> Void)
    {
        repo.getUsers(page: page) { (response, error) in
            
            if (error == nil)
            {
                self.users = response?.data
                completion(true, nil)
            }
            else
            {
                completion(false, error)
            }
        }
    }
}
